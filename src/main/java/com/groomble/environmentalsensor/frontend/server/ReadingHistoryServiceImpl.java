package com.groomble.environmentalsensor.frontend.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.groomble.environmentalsensor.frontend.client.ReadingHistoryService;
import com.groomble.environmentalsensor.frontend.shared.SensorReading;

@SuppressWarnings("serial")
public class ReadingHistoryServiceImpl extends RemoteServiceServlet implements ReadingHistoryService {
	private PreparedStatement sqlStatement;
	
	private static final Logger log = LoggerFactory.getLogger(ReadingHistoryServiceImpl.class);
	
	@Override
	public void init() throws ServletException {
		super.init();
		Connection database = null;
		database = (Connection) getServletContext().getAttribute("connection");
		try {
			sqlStatement = database.prepareStatement("SELECT * FROM SENSORS WHERE TAKEN > ? AND TAKEN < ?");
		} catch (SQLException e) {
			sqlStatement = null;
			log.error("Failed to construct the database statement. Data retrieval may fail. ", e);
			e.printStackTrace();
		}
	}
	
	@Override
	public SensorReading[] getReadingsInTimePeriod(Date begin, Date end, int numberOfReadings) {
		ArrayList<ResultSet> resultSetListHelper = new ArrayList<ResultSet>();
		synchronized(sqlStatement) {
			try {
				sqlStatement.setTimestamp(1, new Timestamp(begin.getTime()));
				sqlStatement.setTimestamp(2, new Timestamp(end.getTime()));
				boolean result = sqlStatement.execute();
				log.info("result availible: "+result);
					resultSetListHelper.add(sqlStatement.getResultSet());
				sqlStatement.clearParameters();
			} catch (SQLException e) {
				log.error("Database query failed.", e);
				e.printStackTrace();
			}
		}
		log.info("List len: "+resultSetListHelper.size());
		ResultSet[] results = resultSetListHelper.toArray(new ResultSet[0]);
		ArrayList<SensorReading> ret = new ArrayList<>();
		for(int i = 0; i < results.length; i++) {
			try {
				log.info("isClosed: "+results[i].isClosed());
				while(results[i].next()) {
					ret.add(new SensorReading(results[i].getFloat(2), results[i].getFloat(3), results[i].getShort(4), results[i].getTimestamp(5)));
				}
			} catch (SQLException e) {
				log.info("SQLException in parsing loop", e);
				e.printStackTrace();
			}
		}
		return ret.toArray(new SensorReading[0]);
	}

}
