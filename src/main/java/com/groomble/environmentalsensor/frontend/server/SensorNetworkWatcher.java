package com.groomble.environmentalsensor.frontend.server;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import com.groomble.environmentalsensor.frontend.shared.SensorReading;

public class SensorNetworkWatcher extends TimerTask implements ServletContextListener, MqttCallback{

	private static final Logger log = LoggerFactory.getLogger(SensorNetworkWatcher.class);
	public SensorNetwork network;

	/**
	 * Parses a single MqttMessage into its component fields, and gives that information to the SensorNetworkWatcher.
	 * @author Groomblecom
	 *
	 */
	public class ParseRunnable implements Runnable {
		String topic;
		MqttMessage message;
		long arrivedTime;
		
		public ParseRunnable(String topic, MqttMessage message, long arrivedTime) {
			this.topic = topic;
			this.message = message;
			this.arrivedTime = arrivedTime;
			log.info("ParserRunnable constructed for message"+topic+":"+message.toString());
		}

		@Override
		public void run() {
			log.info("Running parser.");
			String[] topicParts = topic.split("/");
			log.info("Root topic:"+topicParts[1]); // [0] is a blank string
			log.info("sensor topic:"+topicParts[2]);
			if(!topicParts[1].equals("sensors")) {
				log.info("Dropped unrelated-to-sensors message.");
				return;
			}
			String messageString = message.toString();
			if(topicParts[3].equals("status")) {
				if(messageString.startsWith("ONLINE sent;")) {
					int bootReasonIndex = messageString.indexOf("bootreason:")+"bootreason:".length();
					int bootReason = Integer.valueOf(messageString.charAt(bootReasonIndex));
					network.acceptNewStatus(topicParts[2], SensorNetwork.SensorStatus.ONLINE, bootReason);
					log.info("Accepted a sensor status method.");
				}
			} else if(topicParts[3].equals("data")) {
				float temp = -1;
				float hum = -1;
				int light = -1;
				Date takenDate = new Date(arrivedTime);
				String[] messageParts = messageString.split(",");
				for(String part:messageParts) {
					String[] messagePartsPart = part.split(":");
					String channel = messagePartsPart[0];
					String data = messagePartsPart[1];
					switch(channel) {
					case "msoff":
//						takenDate = new Date(arrivedTime-Double.valueOf(data).longValue());
							break;
					case "temp":temp = Float.valueOf(data);
							break;
					case "humidity":hum = Float.valueOf(data);
							break;
					case "light":light = Double.valueOf(data).intValue();
							break;
					default:
							log.info(channel+" is not a valid data channel. Ignoring.");	
					}
				}
				SensorReading reading = new SensorReading(temp, hum, light, takenDate);
				network.acceptNewReading(topicParts[2], reading);
				log.info("Submitted a new data reading: "+reading.toString());
			} else {
				log.warn("Bad message topic! "+topic+" is not a valid sensor topic");
			}
		}

	}

	/**
	 * What MQTT server to connect to. This is loaded from config; this value is a fallback.
	 */
	private String serverUri = "tcp://localhost:1883";

	/**
	 * Time to wait until giving up on the MQTT server. Presently set to 2 seconds.
	 */
	private static final long MQTT_CONNECTION_TIMEOUT = 2000;
	/**
	 * How often to actively query the sensor network. Presently set to 30 seconds.
	 */
	private static final long SCAN_FREQUENCY = 30*1000;

	static private Timer scanTimer = new Timer("Sensor Network Scan Timer", true);
	private static SensorNetwork defaultNetwork;
	
	/**
	 * used to talk to the MQTT server, both for active querying and passive watching.
	 */
	private MqttAsyncClient server;

	/**
	 * Parses incoming MQTT messages in its own threads to avoid delaying high QoS message acknowledgments
	 */
	private Executor parseExecutor;
	/**
	 * Holds the connection to the H2 database so it can be closed on exit.
	 */
	private Connection database;
	
	public SensorNetworkWatcher() {
		parseExecutor = new ThreadPoolExecutor(1, 2, 5, TimeUnit.MINUTES, new LinkedBlockingDeque<Runnable>(500));
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		try {
			database.commit();
			database.close();
		} catch (SQLException e) {
			log.error("Difficulty closing database connection!", e);
			e.printStackTrace();
		}
		scanTimer.cancel();
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		try {
			FileInputStream configfile = new FileInputStream((String) event.getServletContext().getInitParameter("config"));
			Properties settings = new Properties();
			settings.load(configfile);
			serverUri = settings.getProperty("broker", serverUri);
			String databaseLoc = settings.getProperty("database", "LOCAL");
			if(!"LOCAL".equals(databaseLoc)) {
				Class.forName("org.h2.Driver");	
				database = DriverManager.getConnection(databaseLoc);
			}
		} catch (IOException e1) {
			log.warn("Could not find or parse config file; falling back to default config.", e1);
		} catch (ClassNotFoundException e) {
			log.error("H2 driver not found", e);
			e.printStackTrace();
		} catch (SQLException e) {
			log.error("Getting DB connection SQL problem.", e);
			e.printStackTrace();
		}
		if(database == null) {
			log.info("Using embedded database.");
			database = (Connection) event.getServletContext().getAttribute("connection");
		}
		event.getServletContext().setAttribute("connection", database);
		if(database == null) {
			log.error("Failed to connect to database! data is being lost!");
		}
		//Check to make sure the database has a sensors table
		try {
			PreparedStatement createTable = database.prepareStatement("CREATE TABLE IF NOT EXISTS SENSORS(ID VARCHAR(255), TEMPERATURE DECIMAL, HUMIDITY DECIMAL, LIGHT SMALLINT, TAKEN TIMESTAMP, SEQ BIGINT AUTO_INCREMENT PRIMARY KEY)");
			createTable.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			server = new MqttAsyncClient(serverUri, "Web Interface Monitoring client "+(new Date().getTime()&0xFFFF), new MemoryPersistence()); // MemoryPersistence prevents errors from trying to open a file to write persistence to.
		} catch (MqttException e) {
			e.printStackTrace();
			log.error("Couldn't even construct the MQTT server object.", e);
		}
		server.setCallback(this);
		
		network = new SensorNetwork(database);
		if(defaultNetwork == null ) {
			defaultNetwork = network;
		}
		log.info("Sensor scan timer scheduling");
		scanTimer.scheduleAtFixedRate(this, 0, SCAN_FREQUENCY);
		log.info("scan timer scheduled.");
	}
	
	@Override
	public void run() {
		log.info("Timer run started.");
		if(!server.isConnected()) {
			try {
				MqttConnectOptions options = new MqttConnectOptions();
				options.setCleanSession(true); // needed to avoid hitting issues with the unneeded persistent mechanism.
				options.setServerURIs(new String[]{serverUri});
				server.connect(options).waitForCompletion(MQTT_CONNECTION_TIMEOUT);
				server.subscribe("/sensors/#", 2).waitForCompletion(MQTT_CONNECTION_TIMEOUT);
			} catch (MqttException e) {
				e.printStackTrace();
				log.error("Could not connect to the MQTT server.", e);
				return;
			}
			log.info("Connected to the MQTT server: "+server.isConnected());
		}
		
	}

	@Override
	public void connectionLost(Throwable cause) {
		// TODO Auto-generated method stub
		log.warn("MQTT connection lost! ", cause);
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		log.info("Message arrived for parsing");
		parseExecutor.execute(new ParseRunnable(topic, message, System.currentTimeMillis()));
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub
		
	}

	public static SensorNetwork getDefaultNetwork() {
		return defaultNetwork;
	}

}
