package com.groomble.environmentalsensor.frontend.server;

import java.util.Date;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.groomble.environmentalsensor.frontend.client.ConnectedSensorsService;
import com.groomble.environmentalsensor.frontend.shared.SensorNetworkStatus;
import com.groomble.environmentalsensor.frontend.shared.SensorReading;

@SuppressWarnings("serial")
public class ConnectedSensorsServiceImpl extends RemoteServiceServlet implements ConnectedSensorsService {
	
	/**
	 * The network that this service will provide information about
	 */
	private SensorNetwork network = SensorNetworkWatcher.getDefaultNetwork();
	
	@Override
	public SensorNetworkStatus getConnectedSensors() {
		SensorReading[] lower = new SensorReading[3];
		SensorReading[] upper = new SensorReading[3];
		for(int i = 0; i < lower.length; i++) {
			lower[i] = new SensorReading((float)(Math.random()*20), (float)(Math.random()*20), (int)(Math.random()*511), new Date());
			upper[i] = new SensorReading((float)(Math.random()*20)+20, (float)(Math.random()*20)+20, (int)(Math.random()*511)+512, new Date());
		}
		return network.getStatus();
	}

}
