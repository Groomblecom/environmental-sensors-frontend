package com.groomble.environmentalsensor.frontend.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.groomble.environmentalsensor.frontend.shared.SensorNetworkStatus;
import com.groomble.environmentalsensor.frontend.shared.SensorReading;

/**
 * Represents a network of sensors and the history thereof.
 * @author Groomblecom
 *
 */
public class SensorNetwork {

	/**
	 * Represents a sensor and all the readings that have been taken by this sensor, along with metadata like description and location, if applicable.
	 * @author Groomblecom
	 *
	 */
	public class Sensor {
		private int bootReason;
		public final String ID;
		private List<SensorReading> readings;
		private SensorStatus status;
		/**
		 * Holds the parameterized SQL statement for adding a new datapoint.
		 */
		private PreparedStatement insertNewReading;
		Sensor(String ID) {
			readings = Collections.synchronizedList(new ArrayList<SensorReading>());
			this.ID = ID;
			IDMap.put(ID, this);
			this.status = SensorStatus.ONLINE;
			try {
				insertNewReading = database.prepareStatement("INSERT INTO SENSORS(ID,TEMPERATURE,HUMIDITY,LIGHT,TAKEN) VALUES(?,?,?,?,?)");
			} catch (SQLException e) {
				log.error("Could not create a PreparedStatement. Data will be lost!", e);
				e.printStackTrace();
			}
		}
		/**
		 * Add a reading to this sensor's history.
		 * @param reading the reading to add.
		 */
		public void addReading(SensorReading reading) {
			if(reading == null ) {
				log.error("Null reading!");
				return;
			}
			try {
				synchronized(insertNewReading) {
					insertNewReading.clearParameters();
					insertNewReading.setString(1, ID);
					insertNewReading.setFloat(2, reading.getTemperature());
					insertNewReading.setFloat(3, reading.getHumidity());
					insertNewReading.setShort(4, (short)reading.getLightlevel());
					insertNewReading.setTimestamp(5, new Timestamp(reading.getTakenTime().getTime()));
					insertNewReading.execute();
				}
			} catch (SQLException e) {
				log.error("Could not send new data to database! Data potentially being lost!", e);
				e.printStackTrace();
			}
			readings.add(reading);
		}
		/**
		 * @return the boot reason: -1 for never sent, 1 for power-on, 2 for RST pin (i.e. black button), 4 for watchdog (frozen code)
		 */
		public int getBootReason() {
			return bootReason;
		}
		/**
		 * Gets the latest (by submission, not timestamp) reading.
		 * @return the reading, or null if there are no readings.
		 */
		public SensorReading getMostRecentReading() {
			if(readings.isEmpty()) {
				return null;
			}
			return readings.get(readings.size()-1);
		}
		public void setStatus(SensorStatus status, int bootReason) {
			this.status = status;
			if(bootReason != -1) { //ignore if -1
				this.bootReason = bootReason;
			}
		}
	}
	/**
	 * List of possible states for a sensor to be in.
	 * @author Groomblecom
	 *
	 */
	public enum SensorStatus {
		OFFLINE_LWT,OFFLINE_TIMEOUT,ONLINE,RETIRED,UPGRADING;
	}
	
	private static final Logger log = LoggerFactory.getLogger(SensorNetwork.class);
	/**
	 * Map of all IDs in use in this network to their sensors.
	 */
	private HashMap<String, Sensor> IDMap = new HashMap<String, Sensor>();
	private Connection database;

	/**
	 * Creates a new sensorNetwork which will use the specified database connection to load and save its measurements.
	 * @param database A connection to an H2 database.
	 */
	public SensorNetwork(Connection database) {
		this.database = database;
	}
	/**
	 * Adds a new reading to the appropriate sensor, and updates the appropriate metrics.
	 * @param ID A String UID for the sensor this reading was taken for. If the UID does not exist, a new sensor will be created to hold this measurement.
	 * @param reading a reading taken by to the specified sensor to integrate into that sensor's history.
	 */
	public void acceptNewReading(String ID, SensorReading reading) {
		Sensor recipient = IDMap.get(ID);
		if(recipient == null) {
			log.info("Accepting new sensor "+ID);
			recipient = new Sensor(ID);
		}
		recipient.addReading(reading);
	}
	/**
	 * Sets the status of the sensor specified by ID, or creates it if it does not exist.
	 * @param ID String sensor UID.
	 * @param status See SensorNetwork.SensorStatus enum.
	 * @param bootReason The ESP8266 bootreason: -1 is no change or field not present, 1 is a normal power-on, 2 is a RST pin (attached to black button on this project's associated hardware platform), and 4 is a watchdog timer reset (frozen code)
	 */
	public void acceptNewStatus(String ID, SensorStatus status, int bootReason) {
		if(!IDMap.containsKey(ID)) {
			log.info("Accepting new sensor "+ID);
			new Sensor(ID); // adds itself to the IDMap
		}
		log.info("Accepted a new status for sensor "+ID);
		IDMap.get(ID).setStatus(status, bootReason);
	}
	public SensorNetworkStatus getStatus() {
		int onlineCount = 0;
		if(IDMap.keySet().size() == 0) {
			return new SensorNetworkStatus(new Date(), null, null, 0);
		}
		SensorReading base = ((Sensor) IDMap.values().toArray()[0]).getMostRecentReading();
		SensorReading[] lower = new SensorReading[]{base, base, base};
		SensorReading[] upper = new SensorReading[]{base, base, base};
		for(Sensor s:IDMap.values()) {
			if(s.status == SensorStatus.ONLINE) {
				onlineCount++;
			} else {
				continue;
			}
			SensorReading r = s.getMostRecentReading();
			if(r == null) {
				continue;
			}
			float hum = r.getHumidity();
			float temp = r.getTemperature();
			int light = r.getLightlevel();
			if(lower[0].getTemperature()>temp)lower[0] = r;
			if(upper[0].getTemperature()<temp)upper[0] = r;
			if(lower[1].getHumidity()>hum)lower[1] = r;
			if(upper[1].getHumidity()<hum)upper[1] = r;
			if(lower[2].getLightlevel()>light)lower[2] = r;
			if(upper[2].getLightlevel()<light)upper[2] = r;
		}
		if(onlineCount == 0) {
			log.info("No sensor connected.");
			return new SensorNetworkStatus(new Date(), null, null, onlineCount);
		}
		return new SensorNetworkStatus(new Date(), lower, upper, onlineCount);
	}

}