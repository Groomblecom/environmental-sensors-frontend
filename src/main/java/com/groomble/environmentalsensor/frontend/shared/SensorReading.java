package com.groomble.environmentalsensor.frontend.shared;

import java.io.Serializable;
import java.util.Date;

/**
 * Represents a single reading pulled from a sensor.
 * @author Groomblecom
 *
 */
public class SensorReading implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8585433376002197488L;
	/**
	 * Represents the measured temperature in degrees celsius. Sigfigs irrelevant for this value, which is computed on the sensor (likely with the accuracy of a DHT11). A value of Float.MIN_VALUE indicates an invalid measurement.
	 */
	private float temperature;
	/**
	 * Humidity values as measured by DHT11. A value of Float.MIN_VALUE indicates that a measurement is not valid.
	 */	
	private float humidity;
	/**
	 * Light level: expressed as a value 0-1023. Has a ~5% constant offset because of tolerances of phototransistors. A value of -1 indicates that the measurement is not valid.
	 */
	private int lightlevel;
	/**
	 * Represents the time at which this measurement was taken. It likely represents an average of about 30s-1min of sensor values before that time.
	 */
	private Date takenTime;
	
	public SensorReading(float temperature, float humidity, int lightlevel, Date takenTime) {
		this.temperature = temperature;
		this.humidity = humidity;
		this.lightlevel = lightlevel;
		this.takenTime = takenTime;
	}
	@SuppressWarnings("unused")
	private SensorReading() {
		this.temperature = Float.MIN_VALUE;
		this.humidity = Float.MIN_VALUE;
		this.lightlevel = -1;
		this.takenTime = new Date();
	}

	/**
	 * @return the lightlevel
	 */
	public int getLightlevel() {
		return lightlevel;
	}

	/**
	 * @return the humidity
	 */
	public float getHumidity() {
		return humidity;
	}

	/**
	 * @return the temperature
	 */
	public float getTemperature() {
		return temperature;
	}

	/**
	 * @return the takenTime
	 */
	public Date getTakenTime() {
		return takenTime;
	}
	/**
	 * Returns a human-readable representation of time taken, temp, humidity, light level.
	 */
	public String toString() {
		return "Taken at "+takenTime.toString()+" temp:"+temperature+", hum:"+humidity+", light:"+lightlevel;
	}
}
