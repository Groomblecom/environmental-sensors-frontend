package com.groomble.environmentalsensor.frontend.shared;

import java.io.Serializable;
import java.util.Date;

/**
 * Represents a network status, encoding connected statistics and a basic statistical summary of the last data received.
 * @author Groomblecom
 *
 */
public class SensorNetworkStatus implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1812832040273561676L;
	/**
	 * The number of sensors connected to network at time of snapshot. A value of -1 indicates an invalid snapshot.
	 */
	private int numConnected;
	/**
	 * The minimum temperature, humidity, and light measurements (in that order).
	 */
	private SensorReading[] minimums;
	/**
	 * The maximum temperature, humidity, and light measurements (in that order).
	 */
	private SensorReading[] maximums;
	/**
	 * Represents when the snapshot was taken.
	 */
	private Date takenTime;
	public SensorNetworkStatus(Date takenTime, SensorReading[] minimums, SensorReading[] maximums, int numConnected) {
		this.maximums = maximums;
		this.minimums = minimums;
		this.numConnected = numConnected;
		this.takenTime = takenTime;
	}
	@SuppressWarnings("unused")
	private SensorNetworkStatus() {
		this.takenTime = new Date();
		this.numConnected = -1;
	}
	/**
	 * @return the numConnected
	 */
	public int getNumConnected() {
		return numConnected;
	}
	/**
	 * @return the minimum temperature, humidity, and light measurements (in that order).
	 */
	public SensorReading[] getMinimums() {
		return minimums;
	}
	/**
	 * @return the maximum temperature, humidity, and light measurements (in that order).
	 */
	public SensorReading[] getMaximums() {
		return maximums;
	}
	/**
	 * @return the time when the snapshot was taken.
	 */
	public Date getTakenTime() {
		return takenTime;
	}
}
