package com.groomble.environmentalsensor.frontend.client;

import java.util.Date;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.groomble.environmentalsensor.frontend.shared.SensorReading;

@RemoteServiceRelativePath("readingHistory")
/**
 * Clientside stub for getting historical readings from the server
 * @author Groomblecom
 *
 */
public interface ReadingHistoryService extends RemoteService {
	SensorReading[] getReadingsInTimePeriod(Date begin, Date end, int numberOfReadings);
}
