package com.groomble.environmentalsensor.frontend.client;

import java.util.Date;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.groomble.environmentalsensor.frontend.shared.SensorNetworkStatus;
import com.google.gwt.dom.client.Element;

public class LandingPage implements EntryPoint {
	
	public class RefreshTimer extends Timer {

		@Override
		public void run() {
			AsyncCallback<SensorNetworkStatus> connectedSensorsCallback = new AsyncCallback<SensorNetworkStatus>() {
				@Override
				public void onFailure(Throwable caught) {
					showNetworkFailMessage();
				}
				@Override
				public void onSuccess(SensorNetworkStatus result) {
					numConnectedSpan.setInnerText(result.getNumConnected()+"");
					lastUpdatedTime = result.getTakenTime();
					graph.addNewMeasurement(result.getMaximums()[0]);
					graph.redraw();
				}
			};
			numConnectedProxy.getConnectedSensors(connectedSensorsCallback);
		}

	}

	private static final int REFRESH_RATE = 30000;

	private Element numConnectedSpan;
	private Element lastUpdatedSpan;
	private Date lastUpdatedTime;
	private Timer refreshTimer = new RefreshTimer();

	private ConnectedSensorsServiceAsync numConnectedProxy;

	private GraphWidget graph;

	@Override
	public void onModuleLoad() {
		numConnectedSpan = DOM.getElementById("numconnected");
		lastUpdatedSpan = DOM.getElementById("lastupdated");
		numConnectedProxy = GWT.create(ConnectedSensorsService.class);
		refreshTimer.scheduleRepeating(REFRESH_RATE);
		refreshTimer.run();
		(new Timer() {
			@Override
			public void run() {
				lastUpdatedSpan.setInnerText((int)(new Date().getTime()-lastUpdatedTime.getTime())/1000+"");
			}
		}).scheduleRepeating(1000);
		
		graph = new GraphWidget("Temperature Today", "welcome");
		HTMLPanel.wrap(DOM.getElementById("welcomegraphregion")).add(graph);
	}
	
	private void showNetworkFailMessage() {
		// TODO Auto-generated method stub
		
	}

}
