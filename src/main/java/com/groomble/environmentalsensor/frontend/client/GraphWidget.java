package com.groomble.environmentalsensor.frontend.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.canvas.dom.client.CssColor;
import com.google.gwt.canvas.dom.client.FillStrokeStyle;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import com.groomble.environmentalsensor.frontend.shared.SensorReading;
/**
 * Provides responsive, canvas-based clientside visual graphs.
 * @author Groomblecom
 *
 */
public class GraphWidget extends Composite implements ClickHandler, MouseMoveHandler, ResizeHandler, MouseOutHandler, MouseOverHandler {

	enum AxisOption {
		TEMPERATURE("°"), HUMIDITY("%RH"), LIGHT("%");
		String suffix;
		AxisOption(String suffix) {
			this.suffix = suffix;
		}
		/**
		 * Gets nice human name for axis.
		 * @return a capitalized string
		 */
		public String getPrettyName() {
			String name = name();
			String lower = name.toLowerCase();
			char[] out = lower.toCharArray();
			out[0] = name.charAt(0);
			return new String(out);
		}
	}
	/**
	 * The canvas used to paint the non-text portion of the graph widget
	 */
	private Canvas canvas;
	/**
	 * The string at the top of the graph.
	 */
	private String header;
	private Label headerLabel;
	private float incrementY;
	private int incrementX;
	
	private static final NumberFormat hourFormat = NumberFormat.getFormat("####.#h");
	/**
	 * The earliest possible time to render
	 */
	Date begin;
	/**
	 * The latest possible time to render
	 */
	Date end;
	/**
	 * List of datapoints to graph.
	 */
	private ArrayList<SensorReading> readings = new ArrayList<SensorReading>();
	/**
	 * Holds the header, the canvas, and the bottom information area.
	 */
	private FlowPanel widgetStack;
	private PopupPanel graphInfoPanel;
	private FlowPanel controlBox;
	/**
	 * Allows the fetching of data from the server for a given time-range.
	 */
	private ReadingHistoryServiceAsync historyProxy;
	private Label valueLabel;
	private Label timeLabel;
	private Label dateLabel;
	/**
	 * Holds the current y-axis to render
	 */
	private AxisOption currentAxis = AxisOption.TEMPERATURE;
	private static final DateTimeFormat timeFormat = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_TIME_SHORT);
	private Label yAxisLabel;
	
	@SuppressWarnings("deprecation") // because GWT emulates old calendar methods, but not the new ones.
	/**
	 * Constructs a widget with the specified title.
	 * @param header A string to put at the top to tell the user what this graph is for or shows.
	 * @param cssName a name to put in the class so that this widget's styleName includes 'graph-[cssName]'
	 */
	public GraphWidget(String header, String cssName) {
		this.header = header;
		historyProxy = GWT.create(ReadingHistoryService.class);
		final AsyncCallback<SensorReading[]> updateGraphCallback = new AsyncCallback<SensorReading[]>() {
			@Override
			public void onFailure(Throwable caught) {
				
			}
			@Override
			public void onSuccess(SensorReading[] result) {
				for(SensorReading reading:result) {
					GWT.log(reading.toString());
					addNewMeasurement(reading);
				}
				redraw();
			}
		};
		widgetStack = new FlowPanel();
		
		canvas = Canvas.createIfSupported();
		if(canvas == null) {
			Window.alert("Your browser does not support the Canvas element, so graph will not work. Sorry."+
					"\nWe recommend updating to a newer browser; all modern browsers should work.");
			return;
		}
		headerLabel = new Label(header);
		headerLabel.setStylePrimaryName("graph-"+cssName);
		headerLabel.setStyleDependentName("title", true);
		headerLabel.addStyleName("graph-title");
		controlBox = new FlowPanel() {
		    @Override
		    public void add(Widget child) {
		        super.add(child);
		        Style childStyle = child.getElement().getStyle();
		        childStyle.setDisplay(Display.INLINE_BLOCK);
		        childStyle.setMarginRight(0.5, Unit.EM);
		    }
		};
		controlBox.add(new Label("Show data from "));
		final DateBox fromDateBox = new DateBox();
		controlBox.add(fromDateBox);
		controlBox.add(new Label(" to "));
		final DateBox toDateBox = new DateBox();
		controlBox.add(toDateBox);
		ValueChangeHandler<Date> dateRangeUpdateHandler = new ValueChangeHandler<Date>() {
			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				if(begin != null && end != null && fromDateBox.getValue() != null && toDateBox.getValue() != null &&
						begin.getTime() == fromDateBox.getValue().getTime() &&
						end.getTime() == toDateBox.getValue().getTime()) {
					GWT.log("Skipping unchanged ");
					return;
				}
				begin = fromDateBox.getValue();
				end = toDateBox.getValue();
				if(begin.after(end)) {
					Date temp = begin;
					begin = end;
					end = temp;
					fromDateBox.setValue(begin);
					toDateBox.setValue(end);
				}
				readings.clear();
				historyProxy.getReadingsInTimePeriod(begin, end, 100, updateGraphCallback);
			}
		};
		fromDateBox.addValueChangeHandler(dateRangeUpdateHandler);
		toDateBox.addValueChangeHandler(dateRangeUpdateHandler);
		final ListBox yAxisOptionBox = new ListBox();
		yAxisOptionBox.addItem(AxisOption.TEMPERATURE.getPrettyName(), AxisOption.TEMPERATURE.name());
		yAxisOptionBox.addItem(AxisOption.HUMIDITY.getPrettyName(), AxisOption.HUMIDITY.name());
		yAxisOptionBox.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				currentAxis = AxisOption.valueOf(yAxisOptionBox.getSelectedValue());
				yAxisLabel.setText(currentAxis.getPrettyName()+": ");
				redraw();
			}
		});
		controlBox.add(new Label("Y Axis:"));
		controlBox.add(yAxisOptionBox);
		Button captureButton = new Button("Save Image");
		captureButton.addStyleName("downloadicon");
		captureButton.addStyleName("rightjustify");
		captureButton.addClickHandler(new ClickHandler() {
			private Element dltag = DOM.createElement("a");
			@Override
			public void onClick(ClickEvent event) {
				dltag.setPropertyString("download", "download.png");
				dltag.setPropertyString("href", canvas.toDataUrl("image/png"));
				dltag.dispatchEvent(Document.get().createClickEvent(0, 0, 0, 0, 0, false, false, false, false));
			}
		});
		controlBox.add(captureButton);
		
		graphInfoPanel = new PopupPanel();
		graphInfoPanel.addStyleName("infoPopupContent");
		VerticalPanel graphInfoStack = new VerticalPanel();
		yAxisLabel = new Label("Temperature: ");
		graphInfoStack.add(yAxisLabel);
		valueLabel = new Label("");
		graphInfoStack.add(valueLabel);
		graphInfoStack.add(new Label("Time: "));
		timeLabel = new Label("");
		graphInfoStack.add(timeLabel);
		dateLabel = new Label("");
		graphInfoStack.add(dateLabel);
		graphInfoPanel.add(graphInfoStack);
		widgetStack.add(headerLabel);
		widgetStack.add(canvas);
		widgetStack.add(controlBox);
		initWidget(widgetStack);
		setStyleName("graph-"+cssName);
		addStyleName("graph");
		controlBox.addStyleName("control-box-container");
		Window.addResizeHandler(this);
		canvas.addMouseMoveHandler(this);
		canvas.addMouseOverHandler(this);
		canvas.addMouseOutHandler(this);
		Date midnight = new Date();
		midnight.setHours(0);
		midnight.setMinutes(0);
		midnight.setSeconds(0);
		Date lastMidnight = new Date(midnight.getTime());
		CalendarUtil.addDaysToDate(midnight, 1);
		toDateBox.getDatePicker().setCurrentMonth(midnight);
		toDateBox.setValue(midnight);
		fromDateBox.getDatePicker().setCurrentMonth(lastMidnight);
		fromDateBox.setValue(lastMidnight);
		dateRangeUpdateHandler.onValueChange(null);
	}
	
	/**
	 * Paints a new measurement, and ensures it will be painted on later redraws. This method does not redraw; you must redraw manually.
	 * @param reading the fresh reading to store.
	 */
	public void addNewMeasurement(SensorReading reading) {
		if(reading == null) {
			GWT.log("Not drawing null datapoint.");
			return;
		}
		if(!readings.contains(reading)) {
			readings.add(reading);
		}
	}

	/**
	 * Gives a y-position on the canvas for a value like temperature or humidity
	 * @param value a sensor reading value: see SensorResult. This is used with the values from drawing the axis to provide the location.
	 * @return a ready-to-use location in pixels.
	 */
	private int getYForValue(float value) {
		return (int) (canvas.getCoordinateSpaceHeight()-50-value*incrementY);
	}

	public String getHeader() {
		return header;
	}

	/**
	 * Gives a x-position on the canvas for a given time.
	 * @param hours A time expressed as a number of hours, possibly with decimals.
	 * @return a ready-to-use location in pixels.
	 */
	private int getXForTime(float hours) {
		//50px offset for axis/labels/etc.
		return (int)(50+incrementX*hours);
	}

	@Override
	public void onClick(ClickEvent event) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void onMouseMove(MouseMoveEvent event) {
		int x = event.getClientX()-graphInfoPanel.getElement().getOffsetWidth();
		int y = event.getClientY();
		if(graphInfoPanel.getOffsetHeight()+y > Window.getClientHeight()) {
			y -= graphInfoPanel.getOffsetHeight();
		}
		if(x < 0) {
			x += 10+graphInfoPanel.getOffsetWidth();
		}
		graphInfoPanel.setPopupPosition(x, y);
		valueLabel.setText(getValueForY(event.getRelativeY(canvas.getCanvasElement()))+currentAxis.suffix);
		float time = getTimeForX(event.getRelativeX(canvas.getCanvasElement()));
		Date pointDate = new Date(begin.getTime()+((long)(time*60*60*1000)));
		timeLabel.setText(hourFormat.format(time));
		dateLabel.setText(timeFormat.format(pointDate));
	}

	private float getTimeForX(int x) {
		return (x-50f)/incrementX;
	}

	/**
	 * Gives a no-better-than-sensor-value approximation for a value on the current (i.e. temperature) y-axis from a position.
	 * @param x the canvas-space coordinate to compute from
	 * @return an approximate value
	 */
	private int getValueForY(int y) {
		return (int) ((y-canvas.getCoordinateSpaceHeight()+50)/-incrementY);
	}

	/**
	 * Paints the legends, axis, and labels onto a graph. Uses 50px on the bottom and left and expects no transforms on the stack. Automatically un-transforms everything it does, so the context will be the same as when passed.
	 * @param g2d the Context2D to paint to.
	 */
	private void paintLegend(Context2d g2d) {
		int width = canvas.getCoordinateSpaceWidth();
		int height = canvas.getCoordinateSpaceHeight();
		if(begin == null || end == null) {
			GWT.log("skipping null");
			return;
		}
		FillStrokeStyle lineStyle = CssColor.make(153, 153, 153);
		g2d.setStrokeStyle(lineStyle);
		g2d.save();
		g2d.setFillStyle("#000000");
		g2d.save();
		g2d.translate(0, height-10);
		String[] dateTimeLines = timeFormat.format(begin).split(" ");
		g2d.fillText(dateTimeLines[0], 0, -20);
		g2d.fillText(dateTimeLines[1], 0, -10);
		g2d.translate(50, 0);
		int labelWidth = 40;
		int numIncrements = width/labelWidth-1;
		int hourDifference = (int) -((begin.getTime()-end.getTime())/(1000*60*60));
		float numHoursPerLabel = ((float)hourDifference)/numIncrements;
		int lineFrequency = 3;
		incrementX = (int) (labelWidth/numHoursPerLabel);
		g2d.beginPath();
		for(int i = 0; i <= numIncrements; i += 1) {
			g2d.fillText(hourFormat.format(i*numHoursPerLabel), 0, 0, 100);
			if(i%lineFrequency == 0) {
				g2d.moveTo(0, 0);
				g2d.lineTo(0, -height);
			}
			g2d.translate(labelWidth, 0);
		}
		g2d.stroke();
		g2d.restore();
		g2d.save();
		g2d.translate(10, 0);
		int maxVal = 50;
		int labelFrequency = 5;
		if(currentAxis == AxisOption.HUMIDITY) {
			maxVal = 100;
			labelFrequency = 10;
		}
		incrementY = (height-50)/((float)maxVal);
//		g2d.translate(0, incrementY*labelFrequency);
		g2d.translate(0, height-50);
		g2d.beginPath();
		for(int i = 0; i <= maxVal; i += 1) {
			if(i%labelFrequency==0) {
				g2d.fillText(i+currentAxis.suffix, 0, 10, 100);
				g2d.moveTo(0, 0);
				g2d.lineTo(width, 0);
			}
			g2d.translate(0, -incrementY);
		}
		g2d.stroke();
		g2d.restore();
		g2d.restore();
	}
	
	/**
	 * Sets the text at the top of the graph.
	 * @param header
	 */
	public void setHeader(String header) {
		this.header = header;
		headerLabel.setText(header);
	}

	@Override
	public void onResize(ResizeEvent event) {
		redraw();
	}

	/**
	 * Redraws the entire canvas, calculating new dimensions if necessary.
	 * @return a Context2d, untransformed, so that additional draw operations can be performed.
	 */
	public Context2d redraw() {
		canvas.setHeight(canvas.getOffsetWidth()/16*9+"px");
		canvas.setCoordinateSpaceWidth(canvas.getOffsetWidth());
		canvas.setCoordinateSpaceHeight(canvas.getOffsetHeight());
		Context2d g2d = canvas.getContext2d();
		g2d.setFillStyle("rgb(100%, 100%, 100%)");
		g2d.fillRect(0, 0, canvas.getCoordinateSpaceWidth(), canvas.getCoordinateSpaceHeight());
		g2d.setFillStyle("#E0E9E0");
		g2d.fillRect(50, 0, canvas.getCoordinateSpaceWidth()-50, canvas.getCoordinateSpaceHeight()-50);
		paintLegend(g2d);
		g2d.save();
		g2d.setFillStyle("#FF0000");
		g2d.setStrokeStyle("#FF0000");
		g2d.beginPath();
		boolean jumped = false;
		for(SensorReading reading:readings) {
			Date takenTime = reading.getTakenTime();
			float takenHours = (takenTime.getTime()-begin.getTime())/(1000*60*60);
			int x = getXForTime(takenHours);
			int y = getYForValue(currentAxis==AxisOption.TEMPERATURE?reading.getTemperature():reading.getHumidity());
			g2d.fillRect(x-5, y-5, 10, 10);
			if(!jumped) {
				g2d.moveTo(x, y);
				jumped = true;
			}
			g2d.lineTo(x, y);
		}
		g2d.stroke();
		g2d.restore();
		return g2d;
	}

	@Override
	public void onMouseOver(MouseOverEvent event) {
		graphInfoPanel.show();
		graphInfoPanel.getElement().addClassName("eventpassthrough"); // stop this from getting way of mouse events to canvas.
	}

	@Override
	public void onMouseOut(MouseOutEvent event) {
		graphInfoPanel.hide();
	}

}
