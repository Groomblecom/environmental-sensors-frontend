package com.groomble.environmentalsensor.frontend.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.groomble.environmentalsensor.frontend.shared.SensorNetworkStatus;

@RemoteServiceRelativePath("connectedSensors")
public interface ConnectedSensorsService extends RemoteService {
	public SensorNetworkStatus getConnectedSensors();
}
